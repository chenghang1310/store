package com.cy.store.mapper;

import com.cy.store.entity.Order;
import com.cy.store.entity.OrderItem;
import com.cy.store.service.IProductService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 订单 Mapper 层单元测试
 *
 * @author chenghang
 * @since 2022/8/12 9:36
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class OrderMapperTests {

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private IProductService productService;

    @Test
    public void insertOrder() {
        Order order = new Order();
        order.setUid(39);
        order.setRecvName("小明");
        order.setRecvPhone("1312313123");
        Integer rows = orderMapper.insertOrder(order);
        System.out.println(rows);
    }

    @Test
    public void insertOrderItem() {
        OrderItem orderItem = new OrderItem();
        orderItem.setOid(1);
        orderItem.setPid(10000013);
        orderItem.setTitle(productService.findById(10000013).getTitle());
        Integer rows = orderMapper.insertOrderItem(orderItem);
        System.out.println("rows=" + rows);
    }
}
