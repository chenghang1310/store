package com.cy.store.mapper;

import com.cy.store.entity.Address;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;

/**
 * 收货地址持久层单元测试
 *
 * @author chenghang
 * @since 2022/8/5 15:35
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class AddressMapperTests {

    @Autowired
    private AddressMapper addressMapper;

    @Test
    public void insert() {
        Address address = new Address();
        address.setUid(39);
        address.setPhone("1353254685");
        address.setName("self");
        addressMapper.insert(address);
    }

    @Test
    public void countByUid() {
        Integer count = addressMapper.countByUid(39);
        System.out.println(count);
    }

    @Test
    public void findByUid() {
        List<Address> list = addressMapper.findByUid(39);
        System.out.println(list);
    }

    @Test
    public void findByAid() {
        Address address = addressMapper.findByAid(15);
        System.out.println(address);
    }

    @Test
    public void updateNonDefault() {
        Integer rows = addressMapper.updateNonDefault(39);
        System.out.println(rows);
    }

    @Test
    public void updateDefaultByAid() {
        Integer rows = addressMapper.updateDefaultByAid(14, "管理员", new Date());
        System.out.println(rows);
    }

    @Test
    public void deleteByAid() {
        Integer rows = addressMapper.deleteByAid(14);
        System.out.println(rows);
    }

    @Test
    public void findLastModified() {
        Address address = addressMapper.findLastModified(39);
        System.out.println(address);
    }
}
