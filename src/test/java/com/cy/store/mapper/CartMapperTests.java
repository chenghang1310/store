package com.cy.store.mapper;

import com.cy.store.entity.Cart;
import com.cy.store.vo.CartVo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;

/**
 * 购物车持久层单元测试
 *
 * @author chenghang
 * @since 2022/8/11 11:10
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class CartMapperTests {

    @Autowired
    private CartMapper cartMapper;

    @Test
    public void insert() {
        Cart cart = new Cart();
        cart.setUid(39);
        cart.setPid(10000021);
        cart.setNum(2);
        cart.setPrice(2000L);
        Integer rows = cartMapper.insert(cart);
        System.out.println("rows=" + rows);
    }

    @Test
    public void updateNumByCid() {
        Integer cid = 2;
        Integer num = 10;
        String modifiedUser = "购物车管理员";
        Date modifiedTime = new Date();
        Integer rows = cartMapper.updateNumByCid(cid, num, modifiedUser, modifiedTime);
        System.out.println("rows=" + rows);
    }

    @Test
    public void findByUidAndPid() {
        Integer uid = 39;
        Integer pid = 10000020;
        Cart result = cartMapper.findByUidAndPid(uid, pid);
        System.err.println(result);
    }

    @Test
    public void findVoByUid() {
        List<CartVo> list = cartMapper.findVoByUid(39);
        System.err.println(list);
    }

    @Test
    public void findByCid() {
        Cart cart = cartMapper.findByCid(6);
        System.err.println(cart);
    }

    @Test
    public void findVoByCids() {
        Integer[] cids = {2,4,6,8};
        List<CartVo> list = cartMapper.findVoByCids(cids);
        System.err.println(list);
    }
}
