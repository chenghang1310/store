package com.cy.store.service;

import com.cy.store.entity.Address;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 收货地址模块业务层单元测试
 *
 * @author chenghang
 * @since 2022/8/8 17:08
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class AddressServiceTests {

    @Autowired
    private IAddressService addressService;

    @Test
    public void addNewAddress() {
        Address address = new Address();
        address.setPhone("1353254611");
        address.setName("测试");
        addressService.addNewAddress(39, "管理员", address);
    }

    @Test
    public void setDefault() {
        addressService.setDefault(2, 39, "管理员");
    }

    @Test
    public void delete() {
        addressService.delete(2, 39, "管理员");
    }
}
