package com.cy.store.controller;

import com.cy.store.entity.Order;
import com.cy.store.service.IOrderService;
import com.cy.store.util.JsonResult;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

/**
 * 订单模块控制层
 *
 * @author chenghang
 * @since 2022/8/12 10:37
 */
@RestController
@RequestMapping("orders")
@RequiredArgsConstructor
public class OrderController extends BaseController {

    /**
     * orderService
     */
    private final IOrderService orderService;

    @RequestMapping("create")
    public JsonResult<Order> create(Integer aid,
                                    Integer[] cids,
                                    HttpSession session) {
        // 从Session中取出uid和username
        Integer uid = getuidFromSession(session);
        String username = getUsernameFromSession(session);
        // 调用业务对象执行业务
        Order data = orderService.create(aid, cids, uid, username);
        // 返回成功与数据
        return new JsonResult<>(OK, data);
    }
}
