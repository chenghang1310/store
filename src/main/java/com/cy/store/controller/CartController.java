package com.cy.store.controller;

import com.cy.store.service.ICartService;
import com.cy.store.util.JsonResult;
import com.cy.store.vo.CartVo;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * 购物车模块控制层
 *
 * @author chenghang
 * @since 2022/8/11 13:58
 */
@RestController
@RequestMapping("carts")
@RequiredArgsConstructor
public class CartController extends BaseController {

    /**
     * cartService
     */
    private final ICartService cartService;

    @RequestMapping("add_to_cart")
    public JsonResult<Void> addToCart(Integer pid,
                                      Integer amount,
                                      HttpSession session) {
        cartService.addToCart(
                getuidFromSession(session),
                pid,
                amount,
                getUsernameFromSession(session));
        return new JsonResult<>(OK);
    }

    @RequestMapping({"", "/"})
    public JsonResult<List<CartVo>> getVoByUid(HttpSession session) {
        List<CartVo> data = cartService
                .getVoByUid(getuidFromSession(session));
        return new JsonResult<>(OK, data);
    }

    @RequestMapping("{cid}/num/add")
    public JsonResult<Integer> addNum(@PathVariable("cid") Integer cid,
                                      HttpSession session) {
        Integer data = cartService.addNum(
                cid,
                getuidFromSession(session),
                getUsernameFromSession(session));
        return new JsonResult<>(OK, data);
    }

    @RequestMapping("list")
    public JsonResult<List<CartVo>> getVoByCids(Integer[] cids,
                                                HttpSession session) {
        List<CartVo> data =
                cartService.getVoByCids(getuidFromSession(session), cids);
        return new JsonResult<>(OK, data);
    }
}
