package com.cy.store.controller;

import com.cy.store.entity.District;
import com.cy.store.service.IDistrictService;
import com.cy.store.util.JsonResult;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 获取省市区控制层
 *
 * @author chenghang
 * @since 2022/8/8 19:09
 */
@RestController
@RequestMapping("districts")
@RequiredArgsConstructor
public class DistrictController extends BaseController {

    /**
     * districtService
     */
    private final IDistrictService districtService;

    // districts开头的请求都被拦截到getByParent()方法中
    @RequestMapping({"/", ""})
    public JsonResult<List<District>> getByParent(String parent) {
        List<District> data = districtService.getByParent(parent);
        return new JsonResult<>(OK, data);
    }
}
