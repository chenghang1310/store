package com.cy.store.service;

import com.cy.store.entity.District;

import java.util.List;

/**
 * 获取省市区 Service 接口
 *
 * @author chenghang
 * @since 2022/8/8 18:24
 */
public interface IDistrictService {

    /**
     * 根据父代号来查询区域信息（省市区）
     *
     * @param parent 父代码
     * @return 多个区域的信息
     */
    List<District> getByParent(String parent);

    /**
     * 根据当前code来获取当前省市区的名称
     *
     * @param code 当前code
     * @return 对应省市区的名称
     */
    String getNameByCode(String code);
}
