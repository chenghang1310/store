package com.cy.store.service.impl;

import com.cy.store.entity.District;
import com.cy.store.mapper.DistrictMapper;
import com.cy.store.service.IDistrictService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 获取省市区 Service 接口实现
 *
 * @author chenghang
 * @since 2022/8/8 18:30
 */
@Service
@RequiredArgsConstructor
public class DistrictServiceImpl implements IDistrictService {

    /**
     * districtMapper
     */
    private final DistrictMapper districtMapper;

    @Override
    public List<District> getByParent(String parent) {

        List<District> list = districtMapper.findByParent(parent);
        // 在进行网络数据传输时，为了尽量避免无效数据的传递，可以将无效数据设置为null
        // 可以节省流量的开销，另一方面提升了效率
        for (District d : list) {
            d.setId(null);
            d.setParent(null);
        }
        return list;
    }

    @Override
    public String getNameByCode(String code) {

        return districtMapper.findNameByCode(code);
    }
}
