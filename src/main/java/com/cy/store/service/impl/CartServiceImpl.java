package com.cy.store.service.impl;

import com.cy.store.entity.Cart;
import com.cy.store.mapper.CartMapper;
import com.cy.store.service.ICartService;
import com.cy.store.service.IProductService;
import com.cy.store.service.ex.AccessDeniedException;
import com.cy.store.service.ex.CartNotFoundException;
import com.cy.store.service.ex.InsertException;
import com.cy.store.service.ex.UpdateException;
import com.cy.store.vo.CartVo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * 购物车 Service 接口实现
 *
 * @author chenghang
 * @since 2022/8/11 11:21
 */
@Service
@RequiredArgsConstructor
public class CartServiceImpl implements ICartService {

    /**
     * cartMapper
     */
    private final CartMapper cartMapper;

    /**
     * productService
     */
    private final IProductService productService;

    @Override
    public void addToCart(Integer uid,
                          Integer pid,
                          Integer amount,
                          String username) {
        // 根据参数pid和uid查询购物车中的数据
        Cart result = cartMapper.findByUidAndPid(uid, pid);

        if (result == null) { // 表示该商品没有被添加到购物车中，则进行新增操作
            // 创建一个cart对象
            Cart cart = new Cart();
            // 补全数据
            cart.setUid(uid);
            cart.setPid(pid);
            cart.setNum(amount);
            cart.setPrice(productService.findById(pid).getPrice());
            cart.setCreatedTime(new Date());
            cart.setModifiedTime(new Date());
            cart.setCreatedUser(username);
            cart.setModifiedUser(username);

            // 插入到数据库
            Integer rows = cartMapper.insert(cart);
            if (rows != 1) {
                throw new InsertException("插入过程产生未知的异常");
            }
        } else { // 表示当前商品在购物车已经存在，更新num值
            Integer rows1 = cartMapper.updateNumByCid(
                    result.getCid(),
                    result.getNum() + amount,
                    username,
                    new Date());
            if (rows1 != 1) {
                throw new UpdateException("更新过程产生未知的异常");
            }
        }
    }

    @Override
    public List<CartVo> getVoByUid(Integer uid) {
        return cartMapper.findVoByUid(uid);
    }

    @Override
    public Integer addNum(Integer cid, Integer uid, String username) {
        Cart result = cartMapper.findByCid(cid);
        if (result == null) {
            throw new CartNotFoundException("购物车数据不存在");
        }
        if (!result.getUid().equals(uid)) {
            throw new AccessDeniedException("非法访问");
        }
        Integer newNum = result.getNum() + 1;
        Integer rows = cartMapper.updateNumByCid(cid, newNum, username, new Date());
        if (rows != 1) {
            throw new UpdateException("更新过程产生未知的异常");
        }

        return newNum;
    }

    @Override
    public List<CartVo> getVoByCids(Integer uid, Integer[] cids) {
        List<CartVo> list = cartMapper.findVoByCids(cids);
        // 迭代器
        Iterator<CartVo> it = list.iterator();
        while (it.hasNext()) {
            CartVo cartVo = it.next();
            if (!cartVo.getUid().equals(uid)) { // 表示当前的数据不属于当前的用户
                // 从集合中移除这个元素
                list.remove(cartVo);
            }
        }
        return list;
    }
}