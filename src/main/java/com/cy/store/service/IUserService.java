package com.cy.store.service;

import com.cy.store.entity.User;

/**
 * 用户模块业务层接口
 *
 * @author chenghang
 * @since 2022/7/28 13:53
 */
public interface IUserService {

    /**
     * 用户注册功能
     *
     * @param user 用户的数据对象
     */
    void reg(User user);

    /**
     * 用户登录功能
     * 将当前登录成功的用户数据以当前用户对象的形式进行返回。
     * 状态管理：可以将数据保存在cookie或session中，可以避免重复度很高的数据多次频繁操作数据进行获取
     * （用户名、用户ID--存放在session中，用户头像--存放在cookies中）
     *
     * @param username 用户名
     * @param password 用户的密码
     * @return 当前匹配的用户数据 如果没有则返回null
     */
    User login(String username, String password);

    void changePassword(Integer uid,
                        String username,
                        String oldPassword,
                        String newPassword);

    /**
     * 根据用户的id查询用户的数据
     *
     * @param uid 用户的id
     * @return 用户的数据
     */
    User getByUid(Integer uid);

    /**
     * 更新用户的数据操作
     *
     * @param uid      用户的id
     * @param username 用户的名称
     * @param user     用户对象的数据
     */
    void changeInfo(Integer uid,
                    String username,
                    User user);

    /**
     * 修改用户的头像
     *
     * @param uid      用户的id
     * @param avatar   用户头像的路径
     * @param username 用户名
     */
    void changeAvatar(Integer uid,
                      String avatar,
                      String username);
}
