package com.cy.store.service.ex;

/**
 * 用户数据不存在的异常
 *
 * @author chenghang
 * @since 2022/7/29 9:27
 */
public class UserNotFindException extends ServiceException {

    public UserNotFindException() {
        super();
    }

    public UserNotFindException(String message) {
        super(message);
    }

    public UserNotFindException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserNotFindException(Throwable cause) {
        super(cause);
    }

    protected UserNotFindException(String message,
                                   Throwable cause,
                                   boolean enableSuppression,
                                   boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
