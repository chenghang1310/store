package com.cy.store.service;

import com.cy.store.entity.Address;

import java.util.List;

/**
 * 收货地址模块 Service 接口
 *
 * @author chenghang
 * @since 2022/8/8 10:50
 */
public interface IAddressService {

    /**
     * 新增收货地址功能
     *
     * @param uid      用户的id
     * @param username 用户名
     * @param address  收货地址数据
     */
    void addNewAddress(Integer uid, String username, Address address);

    /**
     * 根据用户的id来获取用户收货地址信息
     *
     * @param uid 用户id
     * @return 收货地址信息
     */
    List<Address> getByUid(Integer uid);

    /**
     * 修改某个用户的某条收货地址数据为默认收货地址
     *
     * @param aid      收货地址id
     * @param uid      用户id
     * @param username 修改者
     */
    void setDefault(Integer aid, Integer uid, String username);

    /**
     * 删除用户选中的收货地址数据
     *
     * @param aid      收货地址id
     * @param uid      用户id
     * @param username 用户名
     */
    void delete(Integer aid, Integer uid, String username);

    /**
     * 根据收货地址数据的id，查询收货地址详情
     *
     * @param aid 收货地址id
     * @param uid 归属的用户id
     * @return 匹配的收货地址详情
     */
    Address getByAid(Integer aid, Integer uid);
}
