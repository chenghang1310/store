package com.cy.store.mapper;

import com.cy.store.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.Date;

/**
 * 用户模块的持久层接口
 *
 * @author chenghang
 * @since 2022/7/28 10:10
 */
//@Mapper
public interface UserMapper {

    /**
     * 插入用户的数据
     *
     * @param user 用户的数据
     * @return 受影响的行数（增删改，都有受影响的行数作为返回值，可以根据返回值来判断是否执行成功）
     */
    Integer insert(User user);

    /**
     * 根据用户名来查询用户的数据
     *
     * @param username 用户名
     * @return 如果找到对应的用户则返回这个用户的数据，如果没有找到则返回null值
     */
    User findByUsername(String username);

    /**
     * 根据用户的uid来修改用户密码
     *
     * @param uid          用户的uid
     * @param password     用户输入的新密码
     * @param modifiedUser 修改的执行者
     * @param modifiedTime 修改数据的时间
     * @return 受影响的行数
     */
    Integer updatePasswordByUid(Integer uid,
                                String password,
                                String modifiedUser,
                                Date modifiedTime);

    /**
     * 根据用户的uid来查询用户的数据
     *
     * @param uid 用户的uid
     * @return 如果找到对应的用户则返回这个用户的数据，如果没有找到则返回null值
     */
    User findByUid(Integer uid);

    /**
     * 根据用户的uid来更新用户的资料信息
     *
     * @param user 用户的数据
     * @return 受影响的行数
     */
    Integer updateInfoByUid(User user);

    /**
     * @param uid          用户的uid
     * @param avatar       用户的头像
     * @param modifiedUser 修改者
     * @param modifiedTime 修改时间
     * @return 受影响的行数
     * @Param("SQL映射文件中#{}占位符的变量名") 解决的问题：当SQL语句的占位符和映射的接口方法参数名不一致时，需要将某个参数
     * 强行注入到某个占位符变量上时，可以使用@Param这个注解来标注映射的关系
     * <p>
     * 根据用户的uid来更新用户的头像
     */
    Integer updateAvatarByUid(
            Integer uid,
            String avatar,
            String modifiedUser,
            Date modifiedTime);
}
