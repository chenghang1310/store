package com.cy.store.mapper;

import com.cy.store.entity.District;

import java.util.List;

/**
 * 获取省市区 Mapper 接口
 *
 * @author chenghang
 * @since 2022/8/8 18:00
 */
public interface DistrictMapper {

    /**
     * 根据父代号查询区域信息
     *
     * @param parent 父代号
     * @return 某个父区域下的所有区域列表
     */
    List<District> findByParent(String parent);

    /**
     * 根据当前code来查询当前省市区的名称
     * @param code 当前code
     * @return 对应省市区的名称
     */
    String findNameByCode(String code);
}
