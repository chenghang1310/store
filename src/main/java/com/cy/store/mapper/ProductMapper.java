package com.cy.store.mapper;

import com.cy.store.entity.Product;

import java.util.List;

/**
 * 商品数据 Mapper 接口
 *
 * @author chenghang
 * @since 2022/8/11 9:35
 */
public interface ProductMapper {

    /**
     * 查询热销商品前四名
     *
     * @return 热销商品前四名列表
     */
    List<Product> findHotList();

    /**
     * 根据商品id查询商品详情
     *
     * @param id 商品id
     * @return 匹配的商品详情，如果没有匹配的数据则返回null
     */
    Product findById(Integer id);
}
