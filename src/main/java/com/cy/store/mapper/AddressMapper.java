package com.cy.store.mapper;

import com.cy.store.entity.Address;

import java.util.Date;
import java.util.List;

/**
 * 收货地址模块持久层接口
 *
 * @author chenghang
 * @since 2022/8/5 14:59
 */
public interface AddressMapper {

    /**
     * 插入用户的收货地址数据
     *
     * @param address 收货地址数据
     * @return 受影响的行数
     */
    Integer insert(Address address);

    /**
     * 根据用户的id统计收货地址数量
     *
     * @param uid 用户的id
     * @return 当前用户的收货地址总数
     */
    Integer countByUid(Integer uid);

    /**
     * 根据用户的id查询用户的收货地址数据
     *
     * @param uid 用户的id
     * @return 收货地址数据
     */
    List<Address> findByUid(Integer uid);

    /**
     * 根据收货地址id查询收货地址数据
     *
     * @param aid 收获地址id
     * @return 收货地址数据，如果没找到则返回null
     */
    Address findByAid(Integer aid);

    /**
     * 根据用户id将其所有收获地址设置为非默认
     *
     * @param uid 用户id
     * @return 受影响行数
     */
    Integer updateNonDefault(Integer uid);

    /**
     * 根据收货地址id将其设置为默认收货地址
     *
     * @param aid          收货地址id
     * @param modifiedUser 修改者
     * @param modifiedTime 修改时间
     * @return 受影响行数
     */
    Integer updateDefaultByAid(Integer aid,
                               String modifiedUser,
                               Date modifiedTime);

    /**
     * 根据收货地址id删除收货地址
     *
     * @param aid 收货地址id
     * @return 受影响的行数
     */
    Integer deleteByAid(Integer aid);

    /**
     * 根据用户id查询当前用户最后一次修改的收货地址
     *
     * @param uid 用户id
     * @return 最后一次修改的收货地址数据
     */
    Address findLastModified(Integer uid);
}
