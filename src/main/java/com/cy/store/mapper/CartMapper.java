package com.cy.store.mapper;

import com.cy.store.entity.Cart;
import com.cy.store.vo.CartVo;

import java.util.Date;
import java.util.List;

/**
 * 购物车数据 Mapper 接口
 *
 * @author chenghang
 * @since 2022/8/11 10:43
 */
public interface CartMapper {

    /**
     * 插入购物车数据
     *
     * @param cart 购物车数据
     * @return 受影响的行数
     */
    Integer insert(Cart cart);

    /**
     * 更新购物车某件商品的数量
     *
     * @param cid          购物车id
     * @param num          更新的数量
     * @param modifiedUser 修改者
     * @param modifiedTime 修改时间
     * @return 受影响的行数
     */
    Integer updateNumByCid(Integer cid,
                           Integer num,
                           String modifiedUser,
                           Date modifiedTime);

    /**
     * 根据用户id和商品id查询购物车中的数据
     *
     * @param uid 用户id
     * @param pid 商品id
     * @return 匹配的购物车数据，如果该用户的购物车中并没有该商品，则返回null
     */
    Cart findByUidAndPid(Integer uid, Integer pid);

    /**
     * 通过用户id查询购物车VO列表
     *
     * @param uid 用户id
     * @return 购物车VO对象列表
     */
    List<CartVo> findVoByUid(Integer uid);

    /**
     * 通过购物车id查询购物车数据
     *
     * @param cid 购物车id
     * @return 购物车数据
     */
    Cart findByCid(Integer cid);

    /**
     * 根据若干个购物车数据id查询详情的列表
     *
     * @param cids 若干个购物车数据id
     * @return 匹配的购物车数据详情的列表
     */
    List<CartVo> findVoByCids(Integer[] cids);
}
